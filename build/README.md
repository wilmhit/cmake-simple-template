At first run you should to:
```
cmake ..
cmake --build .
```

Every build later is done just via:
```
cmake --build .
```
